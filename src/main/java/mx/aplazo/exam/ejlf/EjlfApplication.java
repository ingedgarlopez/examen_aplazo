package mx.aplazo.exam.ejlf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjlfApplication {

	public static void main(String[] args) {
		SpringApplication.run(EjlfApplication.class, args);
	}

}
