package mx.aplazo.exam.ejlf.persistence;

import mx.aplazo.exam.ejlf.entities.PaymentsEntity;

public interface PaymentsDao {

    public void insert(PaymentsEntity payment);
}
