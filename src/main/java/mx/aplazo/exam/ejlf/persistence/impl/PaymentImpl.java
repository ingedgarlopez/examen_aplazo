package mx.aplazo.exam.ejlf.persistence.impl;

import mx.aplazo.exam.ejlf.entities.PaymentsEntity;
import mx.aplazo.exam.ejlf.persistence.PaymentsDao;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository
public class PaymentImpl implements PaymentsDao {

    private EntityManager em = null;

    @PersistenceContext
    public void setEntityManager(EntityManager em){
        this.em = em;
    }


    @Override
    @SuppressWarnings("uncheked")
    @Transactional
    public void insert(PaymentsEntity payment) {
        em.merge(payment);
    }
}
