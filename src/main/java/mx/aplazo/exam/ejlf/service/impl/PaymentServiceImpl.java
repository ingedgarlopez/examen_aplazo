package mx.aplazo.exam.ejlf.service.impl;

import mx.aplazo.exam.ejlf.entities.PaymentsEntity;
import mx.aplazo.exam.ejlf.persistence.PaymentsDao;
import mx.aplazo.exam.ejlf.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentsDao paymentsDao;

    @Override
    public void insert(PaymentsEntity payment) {
        paymentsDao.insert(payment);
    }
}
