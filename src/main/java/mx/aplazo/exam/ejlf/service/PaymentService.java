package mx.aplazo.exam.ejlf.service;

import mx.aplazo.exam.ejlf.entities.PaymentsEntity;

public interface PaymentService {

    public void insert(PaymentsEntity payment);
}
