package mx.aplazo.exam.ejlf.exception;

public class CustomException extends Exception{
    private String message = null;
    private String code = null;

    public CustomException(String message, String code) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }
}
