package mx.aplazo.exam.ejlf.endpoint;

import mx.aplazo.exam.ejlf.entities.PaymentsEntity;
import mx.aplazo.exam.ejlf.model.PaymentRequest;
import mx.aplazo.exam.ejlf.service.PaymentService;
import mx.aplazo.exam.ejlf.service.impl.PaymentServiceImpl;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@RestController
public class PaymentsController {

    @Autowired
    PaymentService paymentService;

    @PostMapping(path = "/calculate/payments", produces = "application/json; charset=utf-8")
    public String execute(@RequestBody PaymentRequest payload) {
        JSONObject response = new JSONObject();
        try {
            //Calculamos el interes
            Double interest = this.calculateInterest(payload);

            //Generamos lista de pagos iguales entre el plazo
            JSONArray paymentList = this.generateListPayments(payload.getAmount()+interest, payload.getTerms());
            response.put("payments", paymentList);

            PaymentsEntity entity = new PaymentsEntity();
            entity.setRequest(payload.toString());
            entity.setResponse(response.toString());
            entity.setCdate(Calendar.getInstance().getTime());

            paymentService.insert(entity);

            return response.toString(1);
        } catch (Exception ex) {
            ex.printStackTrace();
            response = new JSONObject();
            response.put("exception_error", "service_error");
            response.put("code", "000");
        }

        return response.toString(1);
    }

    /**
     * Metodo para calcular el total a pagar con interes simple
     * formula interes = amount * rate * terms
     * @param data
     * @return
     */
    private Double calculateInterest(PaymentRequest data) {
        Double interest = data.getAmount() * data.getRate() * data.getTerms();
        DecimalFormat formatter = new DecimalFormat("#0.00");
        return Double.parseDouble(formatter.format(interest));
    }

    /**
     * Metodo para generar la lista de pagos
     * @param total
     * @param plazo
     * @return
     */
    private JSONArray generateListPayments(Double total, Integer plazo) {
        Double partialPayment = total / plazo;
        JSONArray list = new JSONArray();

        DecimalFormat formatter = new DecimalFormat("#0.00");

        for (int i = 1; i <= plazo; i++) {
            JSONObject payment = new JSONObject();
            payment.put("payment_number", i);
            payment.put("amount", Double.parseDouble(formatter.format(partialPayment)));
            //Falta el payment date
            payment.put("payment_date", this.getPaymentDate(i));
            list.put(payment);
        }
        return list;
    }

    private String getPaymentDate(int paymentNumber) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, (7*paymentNumber));

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        return formatter.format(cal.getTime());
    }
}
