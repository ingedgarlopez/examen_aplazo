package mx.aplazo.exam.ejlf.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="payments")
public class PaymentsEntity {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name="request")
    private String request;

    @Lob
    @Column(name = "response", length = 5000)
    private String response;

    @Column(name="cdate")
    private Date cdate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Date getCdate() {
        return cdate;
    }

    public void setCdate(Date cdate) {
        this.cdate = cdate;
    }
}
